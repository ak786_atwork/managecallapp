package com.care.managecallapp

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        askPermissicon()
    }

    private fun askPermissicon() {
        ActivityCompat.requestPermissions(this,
            arrayOf( Manifest.permission.CALL_PHONE, Manifest.permission.MODIFY_PHONE_STATE, Manifest.permission.READ_PHONE_STATE),
            100)
    }
}
